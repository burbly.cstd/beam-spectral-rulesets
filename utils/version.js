const fs = require('fs')

const log = console.log
const err = console.error
const version = process.argv.length > 2 ? process.argv[2] : null

const init = () => {
	if (version && version.indexOf('release-') !== -1) {
		const items = fs.readFileSync('./package.json')
		if (items) {

			const versionChunk = version.split('-')
			if (versionChunk.length === 2) {
				log(`Updating package version to ${versionChunk[1]}.`)

				const parsedItems = JSON.parse(items)
				parsedItems.version = versionChunk[1]
				fs.writeFileSync('./package.json', JSON.stringify(parsedItems, null, 2), 'utf-8')

				log('📦 Ready for release!')
			} else {
				err('ERR: Unable to update version number.')
			}

		} 
	}
}

init()