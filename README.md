[![pipeline status](https://gitlab.com/beam-core/beam-spectral-rulesets/badges/main/pipeline.svg)](https://gitlab.com/beam-core/beam-spectral-rulesets/-/commits/main) [![coverage report](https://gitlab.com/beam-core/beam-spectral-rulesets/badges/main/coverage.svg)](https://gitlab.com/beam-core/beam-spectral-rulesets/-/commits/main)


👷 🚧 in development 🚧 👷

Custom [Spectral API Linter](https://github.com/stoplightio/spectral) ruleset for [Beam RESTful API Guidelines](https://gitlab.com/beam-core/cloud-api-specification).


#### Usage
Install via Yarn
```bash
yarn add @burbly.cstd/spectral-rulesets
echo "extend: [@burbly.cstd/spectral-rulesets]" > .spectral.yml
npx @stoplight/spectral lint ./example.yml
```

Or Reference via Gitlab URL
```bash
echo "extend: [https://gitlab.com/beam-core/beam-spectral-rulesets/-/raw/main/ruleset.json]" > .spectral.yml
npx @stoplight/spectral lint ./example.yml
```
#### Setup
This project has few prerequisites
- Git
- Node.JS
- Yarn/NPM

To download this project, download the source code and install the dependencies
```bash
git clone git@gitlab.com:beam-core/beam-spectral-rulesets.git
cd beam-spectral-rulesets
yarn install

# Following TDD method, run test in watch mode while writing your functions
yarn test:watch
```
> 💡 Developers are advised to use TDD (test driven development) method for development

```text
lint - Validates the function files and all JS files
lint:fix - Fixes all lint error
test: - Run test once
test:ci - Run test/coverage and generate a test result as HTML file
test:watch - Run test in watch mode
```

#### Adding a new rule
1. Identify which rule or collection of rules needs to be implemented
2. Create a file in the `functions` directory with the function name and add the rule function in it.
> On success, the ruleset function shouldn't return anything. A comprehensive error message should be returned and only potential exceptions should be handled.
3. In the directory `__tests__` include a test for the function you just created that
    - Takes in the expected function parameter
    - Intentionally breaks the function and returns all potential exceptions
    - Intentionally passes the function and does not have a return value
4. Add the rule to the `README.md` table following the existing pattern
5. Create a PR

There are some other scripts in the `package.json` file but they are used by the CI/CD process.

#### **Currently supported rules**
:small_blue_diamond: - Implemented  
:small_orange_diamond: - Yet to be implemented

| id             | title                                                                     | Function | Test | Shipped |
|----------------|---------------------------------------------------------------------------|----------|------|---------|
| [#001](https://gitlab.com/beam-core/beam-spectral-rulesets/-/blob/main/functions/uuid.js)   | `valid-uuid-value` - Makes sure all ID field is string and conforms to UUID v4 requirements | :small_blue_diamond: | :small_blue_diamond: | :small_blue_diamond: |