import uuid from '../functions/uuid'

const INVALID_STRING = 1
const INVALID_UUID = 'ab435524-17f0-11ed-bfce-53ca0cdecee9'
const VALID_UUID_V4 = '3bf3a6a0-6a4e-4fbd-a780-4272868d2945'

describe('valid-uuid-value', () => {
	it('UUID is string', () => {
		expect(uuid(INVALID_STRING))
			.toHaveProperty(
				[0, 'message'], 'Value must be a string.'
			)
	})

	it('UUID is not valid', () => {
		expect(uuid(INVALID_UUID))
			.toHaveProperty(
				[0, 'message'], 'Value must be of type UUID v4.'
			)
	})

	it('UUID v4 is valid', () => {
		expect(uuid(VALID_UUID_V4)).toEqual()
	})
})
