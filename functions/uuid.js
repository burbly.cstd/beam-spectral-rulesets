'use strict'

const uuidRegex = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i

export default function (value) {
	if (typeof value !== 'string') {
		return [{ message: 'Value must be a string.' }]
	}

	if (!uuidRegex.test(value)) {
		return [{ message: 'Value must be of type UUID v4.' }]
	}
}
